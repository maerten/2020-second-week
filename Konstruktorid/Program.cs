﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktorid
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene.Create("38112051234").Nimi = "Henn"; //Factory pattern
        }
    }

    class Inimene
    {
        public static Dictionary<string, Inimene> Inimesed = new Dictionary<string, Inimene>();
        public string Nimi { get; set; }
        public readonly string IK;

        public Inimene(string ik) //konstruktor
        {
            IK = ik;
            if (!Inimesed.ContainsKey(ik)) Inimesed.Add(ik, this);//lisab nimekirja siis kui juba ees ei ole
        }

        public Inimene(string ik, string nimi) //konstruktori overload
            : this(ik) //konstruktorite sidumine
        {
            Nimi = nimi;
        }

        public static Inimene Create(string ik) //inimene luuakse meetodiga (konstruktor tehakse privaatseks) - kui ik on olemas, antakse olol inimene, muidu luukase uus
        {
            if (Inimesed.ContainsKey(ik)) return Inimesed[ik];
            else return new Inimene(ik);
        }

        public static Inimene Leia(string ik) => Inimesed.ContainsKey(ik) ? Inimesed[ik] : null;
    }
}
