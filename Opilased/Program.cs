﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opilased
{


    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> õpilased = new List<Inimene>();
            foreach (var rida in System.IO.File.ReadAllLines(@"..\..\Õpilased.txt"))
            {
                var osad = rida.Replace(", ", ",").Split(',');
                if (osad.Length > 1) õpilased.Add(new Inimene { Nimi = osad[1], Isikukood = osad[0] });
            }

            foreach (var i in õpilased) Console.WriteLine($"{i.Nimi} vanus on {i.Vanus()}");            
        }
    }

    class Inimene
    {
        public string Nimi;
        public string Isikukood;

        public DateTime Sünniaeg()
        {
            int sajand = 1800;
            switch (Isikukood.Substring(0, 1))
            {
                case "3": case "4": sajand = 1900; break;
                case "5": case "6": sajand = 2000; break;
            }

            return new DateTime(
                // variant Elvistega
                //(Isikukood[0] == '1' || Isikukood[0] == '2' ? 1800 :
                // Isikukood[0] == '3' || Isikukood[0] == '4' ? 1900 : 2000) + 
                sajand +
                int.Parse(Isikukood.Substring(1, 2)),     // aasta
                int.Parse(Isikukood.Substring(3, 2)),     // kuu
                int.Parse(Isikukood.Substring(5, 2))      // päev
                );
        }

        public int Vanus() => (DateTime.Today - Sünniaeg()).Days * 4 / 1461;


        public override string ToString()
        => $"{Isikukood} - {Nimi}";
    }

}