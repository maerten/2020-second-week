﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyBirthday
{
    class Program
    {
        static void Main(string[] args)
        {

            string ik="";
            bool õnnitlemata = true;

            do
            {
                Console.Write("Sisesta palun oma isikukood: ");
                ik = Console.ReadLine();
                try
                {
                    DateTime sünnipäev = new DateTime(2020, int.Parse(ik.Substring(3, 2)), int.Parse(ik.Substring(5, 2)));
                    if (DateTime.Today == sünnipäev) Console.WriteLine("Palju õnne sünnipäevaks!");
                    if (DateTime.Today > sünnipäev) Console.WriteLine("Palju õnne sünnipäevaks tagantjärele!");
                    õnnitlemata = false;
                }
                catch
                {
                    Console.WriteLine("Siit pole võimalik lugeda sünnipäeva.");
                } 
            } while (õnnitlemata);
        }
    }
}
