﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchSpordipaev
{
    class ProtRida
    {
        public string Loom;
        public double Distants;
        public double Aeg;
        public double Kiirus => Distants / Aeg;
    }
}
