﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchSpordipaev
{
    class Program
    {
        static void Main(string[] args)
        {                    
            string filename = "..\\..\\spordipäeva protokoll.txt";

            List<double> distantsid = new List<double>();            
            List<ProtRida> protRead = new List<ProtRida>();
            double distants = 0;
            bool failiPole = true;
            string[] failiRead = new string[100];
            bool veadFailis = false;

            do
            {
                try
                {
                    failiRead = System.IO.File.ReadAllLines(filename);
                    failiPole = false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Faili ei leitud. Palun aseta allnimetatud fail alltoodud kausta ning vajuta Enter:");
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                } 
            } while (failiPole);

            do
            {
                veadFailis = false;
                distants = 0;
                distantsid.Clear();
                for (int i = 1; i < failiRead.Length; i++)
                {
                    string[] reaosad = failiRead[i].Split(',');
                    if (reaosad.Length > 1)
                    {
                        try
                        {
                            distants = double.Parse(reaosad[1].Trim());
                            if (!distantsid.Contains(distants)) distantsid.Add(distants);
                            protRead.Add(new ProtRida { Loom = reaosad[0].Trim(), Distants = distants, Aeg = double.Parse(reaosad[2].Trim()) });
                        }
                        catch
                        {
                            Console.WriteLine($"NB! rida nr {i} on vigane: ({failiRead[i]}). Rida peab olema formaadis: (jooksja, distants(m), aeg(s).");
                            veadFailis = true;
                            continue;
                        }
                    }
                }
                if (veadFailis)
                {
                    Console.WriteLine("Paranda vead failis ning vajuta siis Enter: ");
                    Console.ReadLine();
                    failiRead = System.IO.File.ReadAllLines(filename);
                }
            } while (veadFailis);

            double kiirus;
            int jookseDistantsil;

            for (int i = 0; i < distantsid.Count; i++)
            {
                jookseDistantsil = 0;
                kiirus = 0;
                foreach (var p in protRead)
                {
                    if (p.Distants == distantsid[i] && p.Aeg>0)
                    {
                        kiirus += p.Kiirus;
                        jookseDistantsil++;
                    }
                }
                Console.WriteLine($"Keskmine kiirus distantsil {distantsid[i]} m on {kiirus/jookseDistantsil:F2} m/s");
            }
        }
    }
}
