﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene();
            henn.Nimi = "Henn Sarv";
            henn.Vanus = 64;

            Console.WriteLine(henn);

            Inimene ants = new Inimene() { Nimi = "Ants Saunamees", Vanus = 80 };

            List<Inimene> rahvas = new List<Inimene>
            {
                new Inimene {Nimi = "Joosep"},
                new Inimene {Nimi = "Teele"},
                new Inimene {Nimi = "Arno"},
                henn,
                Inimene ants = new Inimene{Nimi = "Maris", kaasa= henn}

            };
        }
    }

    class Inimene
    {
        public string Nimi; // vaikimisi tühi string
        public int Vanus; //vaikimisi 0
        public Inimene kaasa; // vaikimisi null

        public override string ToString() => $"Inimene {Nimi} vanusega {Vanus}";

        Inimene[] inimesed = new Inimene[10];
        List<Inimene> teised = new List<Inimene>();

        

        //teised.Add(new Inimene {Nimi = "Ants"}


    }
}
