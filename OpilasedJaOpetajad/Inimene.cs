﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpilasedJaOpetajad
{
    enum Sugu { naine, mees}
    enum LapseSugu { tüdruk, poiss}
    class Inimene
    {
        public string Nimi { get; set; }
        public string Isikukood { get; set; }

        public DateTime Sünniaeg =>
            Isikukood=="" ? new DateTime():
            new DateTime(
                (Isikukood[0] == '5' || Isikukood[0] == '6' ? 2000 :
                Isikukood[0] == '3' || Isikukood[0] == '4' ? 1900 :
                1800)+
                int.Parse(Isikukood.Substring(1, 2)),     // aasta
                int.Parse(Isikukood.Substring(3, 2)),     // kuu
                int.Parse(Isikukood.Substring(5, 2))      // päev
                );

        public Sugu Sugu => (Sugu)(Isikukood[0] % 2);
        public LapseSugu LapseSugu => (LapseSugu)(Isikukood[0] % 2);
        public string SuguStr => Vanus >= 20 ? Sugu.ToString() : LapseSugu.ToString();
        public int Sünnipäev => Sünniaeg.DayOfYear;
        public int Sünnikuu => Sünniaeg.Month;
        public int Vanus => (DateTime.Today - Sünniaeg).Days * 4 / 1461;

        public static int Age(this Inimene p) => (DateTime.Today - p.Sünniaeg).Days * 4 / 1461; //(this Inimene p) - laiendusfunktsioon (extension function)

        public override string ToString()
            => $"{Nimi} (IK {Isikukood}) on {Vanus}-aastane {SuguStr}, kes on sündinud {Sünniaeg:dd.MMMM.yyyy}";
    }
}
