﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace OpilasedJaOpetajad
{
    class Program
    {
        static void Main(string[] args)
        {            
            List<Inimene> inimesed = new List<Inimene>();

            //Järgmise nädala kontroll
            DateTime Päev1 = new DateTime(2020, 03, 08);
            DateTime Päev2 = new DateTime(2020, 03, 15);
            int NK = Päev2.DayOfYear - Päev1.DayOfYear - (int)Päev1.DayOfWeek;
            Console.WriteLine(NK);

            ReadPeople("..\\..\\Õpilased.txt", inimesed);
            ReadPeople("..\\..\\Õpetajad.txt", inimesed);

            int päevAastasPraegu = DateTime.Now.DayOfYear;
            int kuuPraegu = DateTime.Now.Month;
            int nädalaPäevPraegu = (int)DateTime.Now.DayOfWeek;

            foreach (Inimene i in inimesed)
            {
                //if (kuuPraegu + 1 == i.Sünnikuu) Console.WriteLine(i);
                int nädalaKontroll = i.Sünnipäev - päevAastasPraegu - nädalaPäevPraegu;
                if (nädalaKontroll <= 6 && nädalaKontroll >= 0) Console.WriteLine(i);
            }
        }

        static void ReadPeople(string fail, List<Inimene> list)
        {
            foreach (var rida in System.IO.File.ReadAllLines(fail))
            {
                var osad = rida.Split(',');
                if (osad.Length>1) //ignoreerib tühje ridu failis
                list.Add(new Inimene { Isikukood = osad[0].Trim(), Nimi = osad[1].Trim() });
            }
        }
    }
}
