﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpordipaevKlassidega
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\spordipäeva protokoll.txt";
            var loetudRead = System.IO.File.ReadAllLines(filename);
            List<Tulemus> protokoll = new List<Tulemus>();
            Dictionary<int, Distants> distantsid = new Dictionary<int, Distants>();

            string kiireimJooksja = "";
            double suurimKiirus = 0;

            for (int i = 1; i < loetudRead.Length; i++)
            {
                var rida = loetudRead[0].Split(',');
                string nimi = rida[0];
                int distant = int.Parse(rida[1]);
                double aeg = double.Parse(rida[2]);
                double kiirus = distant / aeg;

                protokoll.Add(new Tulemus
                {
                    Nimi = nimi,
                    Distants = distant,
                    Aeg = aeg,
                    Kiirus = kiirus
                });

                if (kiirus > suurimKiirus) (suurimKiirus, kiireimJooksja) = (kiirus, nimi);

                if (!distantsid.ContainsKey(distant)) distantsid.Add(distant, new Distants
                {
                    Pikkus = distant,
                    Parim = nimi,
                    Kiirus = kiirus
                });

                if(kiirus>distantsid[distant].Kiirus)
                {
                    distantsid[distant].Parim = nimi;
                    distantsid[distant].Kiirus = kiirus;
                }
            }
        }
    }

    class Tulemus
    {
        public string Nimi;
        public int Distants;
        public double Aeg;
        public double Kiirus;

        public override string ToString() => $"{Nimi} jooksis {Distants} ajaga {Aeg:F2}";

    }

    class Distants
    {
        public int Pikkus;
        public double Kiirus;
        public string Parim;

        public override string ToString() => $"Distantsi {Pikkus} parim oli {Parim} kiirusega {Kiirus:F2}";

    }
}
