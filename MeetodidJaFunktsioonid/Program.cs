﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetodidJaFunktsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime sünnipäev = new DateTime(1955, 3, 7);

            Trüki(sünnipäev); //meetodi väljakutse

            Console.WriteLine(Vanus(sünnipäev)); //funktsiooni kasutamine avaldises

            if (Vanus(sünnipäev) > 18) Console.WriteLine("Talle võib viina müüa");
        }

        static int Vanus(DateTime päev) //funktsioon: tal on vastuse tüüp ja ta arvutab ning returnib midagi
        {
            var v= (DateTime.Today - päev).Days * 4 / 1461;
            return v;
        }

        static void Trüki(DateTime päev) //meetod teeb midagi, vastuse tüüp on void
        {
            Console.WriteLine(päev.ToString("(ddd) dd.MMMM.yyyy"));
        }
    }

    class Inimene
    {
        static int inimesteArv = 0; //static on klassi küljes ja kõigi inimeste peale ühine

        public int Number = ++inimesteArv;
        public string Nimi;
        public DateTime Sünniaeg;

        public override string ToString()
            => $"{Number} {Nimi} kes on sündinud {Sünniaeg:dd.MMMM.yyyy}";

        //public int AgeInstants

    }
}
