﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KleineIntermedium
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(PalgaArvestus.Tulumaks(800));
            List<Inimene> inimesed = new List<Inimene>
            {
                new Inimene {Nimi = "Henn", Palk = 10000},
                new Inimene {Nimi = "Ants", Palk = 800},
                new Inimene {Nimi = "Peeter", Palk = 100}
            };

            foreach (var i in inimesed) Console.WriteLine($"{i.Nimi} saab {i.Palk - i.Tulumaks()}");
        }
    }

    static class PalgaArvestus
    {
        public static decimal Maksuvaba { get; set; } = 500M;
        public static decimal Maksumäär { get; set; } = 0.2M;

        public static decimal Tulumaks(decimal summa) => summa < Maksuvaba ? 0 : (summa - Maksuvaba) * Maksumäär;
    }

    class Inimene
    {
        public string Nimi = { get; set; }
        public decimal Palk = { get; set; }

        public decimal Tulumaks() => PalgaArvestus.Tulumaks(Palk);
    }
}
