﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ma tahaks jagada");

            try
            {
                Console.Write("anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());

                Console.Write("anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());
                if (teine == 0) throw new Exception("Nulliga ei saa jagada");

                int tulemus = yks / teine;

                Console.WriteLine($"tulemus {tulemus}");
            }
            catch (Exception e)
            {
                Console.WriteLine("Midagi juhtus");
                Console.WriteLine(e.Message);
                throw; //veateade suunatakse "korrus kõrgemale" süsteemile, kuid tuleb "finally" programmi tagasi
            }
            finally //juhtimine antakse tagasi programmile
            {
                Console.WriteLine("Tänaseks lõpetame jagamise");
            }

        }
    }
}
