﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeneOpilaneOpetaja
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] failid = { "..\\..\\Õpilased.txt", "..\\..\\Õpetajad.txt" };
            List<Inimene> inimesed = new List<Inimene>();
            List<Õpilane> õpilased = new List<Õpilane>();
            List<Õpetaja> õpetajad = new List<Õpetaja>();

            foreach (string fail in failid)
            {
                foreach (var rida in System.IO.File.ReadAllLines(fail))
                {                    
                    var osad = rida.Replace(", ", ",").Split(',');
                    if (osad.Length > 1)
                    {
                        inimesed.Add(new Inimene
                        {
                            IK = osad[0],
                            Nimi = osad[1],
                        });
                        if (fail == failid[0]) õpilased.Add(new Õpilane
                        {
                            IK = osad[0],
                            Nimi = osad[1],
                            Klass = osad[2]
                        });
                        if (fail == failid[1]) õpetajad.Add(new Õpetaja
                        {
                            IK = osad[0],
                            Nimi = osad[1],
                            Aine = osad[2],
                            Klass = (osad.Length == 4) ?  osad[3]: "mitte ühegi"
                        });
                    }
                }
            }

            Console.WriteLine("Inimesed");
            foreach (Inimene i in inimesed)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("Õpilased");
            foreach (Inimene i in õpilased)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("Õpetajad");
            foreach (Inimene i in õpetajad)
            {
                Console.WriteLine(i);
            }
        }
    }

    enum Sugu { Naine, Mees }
    class Inimene
    {
        public string IK { get; set; } = "";
        public string Nimi { get; set; } = "";
        public Sugu Sugu => (Sugu)(int.Parse(IK.Substring(0,1)) % 2);
        public override string ToString() => $"{Sugu} {Nimi} ({IK})";

    }

    class Õpilane : Inimene
    {
        public string Klass { get; set; }
        public override string ToString() => $"{Klass} klassi õpilane {Nimi} ({IK})";
    }

    class Õpetaja : Inimene
    {
        public string Aine { get; set; }
        public string Klass { get; set; } = "";
        public override string ToString() => $"{Aine} õpetaja {Nimi} ({IK}), {Klass} klassi juhataja";
    }
}
