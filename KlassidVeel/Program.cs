﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidVeel
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene { Nimi = "Henn Sarv", IK = "35503070001" }; 
            Console.WriteLine(henn);

            foreach (var i in inimesed) Console.WriteLine(i);


            ////Klass inimene peab olema Disposable
            //using (Inimene ajutine = new Inimene { Nimi = "keegi", IK="00000000000"})
            //{
            //    //Ajutine inimene kehtib ainult siin plokis
            //}

            Console.WriteLine(Liida(4,7)); //positsioonilised argumendid
            Console.WriteLine(Liida(x:4, z:7)); //nimelised argumendid (kui paljud on optional)
        }

        
        //Sama nimega, erinev signatuur - overload'imine
        static int Liida(int x, int y)
        {
            return x + y;
        }

        static int Liida(int x, int y=0, int z=0) //optional parameter peale kohustuslikke parameetreid - sarnane efekt kui overload'iga)
        {
            return 4*x + 2*y + z;
        }

        static int Liida(int x, params int[] muud) //parameetriks on massiiv - saab panna kuitahes palju parameetreid
            //esimene parameeter on siiski int, sest ilma ühegi liitetavaga summat ei ole
        {
            int sum = x;
            foreach (var s in muud) sum += s;
            return sum;
        }

    }
}
