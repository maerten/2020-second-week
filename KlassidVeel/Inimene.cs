﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidVeel
{
    class Inimene
    {
        static int loendur = 0;
        static List<Inimene> inimesed = new List<Inimene>();
        public static IEnumerable<Inimene> Inimesed => 
        
        public string Nimi;
        public string IK ="";
        public int NR = ++loendur;

        public Inimene()  //Konstruktor
        {
            this.Nimi = "";  //this'i võib ka ära jätta
            this.IK = "10101010000";
            inimesed.Add(this);
        }
        
        public DateTime Sünniaeg()
        {
            return 
                (IK.Length<11) ? DateTime.Now :
                new DateTime(
                IK[0] == '1' || IK[0] == '2' ? 1800 :
                IK[0] == '3' || IK[0] == '4' ? 1900 : 2000 +
                int.Parse(IK.Substring(1, 2)),
                int.Parse(IK.Substring(3, 2)),
                int.Parse(IK.Substring(5, 2))
                );
        }

        public override string ToString() => $"{Nimi} sündinud {Sünniaeg()} dd.MMM.yyyy";

    }
}
